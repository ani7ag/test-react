import React, { Component } from "react";
import HeaderContainer from "./containers/HeaderContainer";
import ArticleListContainer from "./containers/ArticleListContainer";
import Modal from "react-modal";
import globalStyles from "./styles/Global.styles";

class App extends Component {
  componentDidMount() {
    Modal.setAppElement(this.el);
  }

  render() {
    return (
      <div ref={ref => (this.el = ref)} className={`${globalStyles}`}>
        <HeaderContainer />
        <ArticleListContainer />
      </div>
    );
  }
}

export default App;
