import React, { Component } from "react";
import PropTypes from "prop-types";
import articleStyles from "../styles/Article.styles";

/** Article */
class Article extends Component {
  render() {
    return (
      <div className={`${articleStyles}`}>
        {this.props.image && (
          <div className={`article__image`}>
            <img src={this.props.image} alt={this.props.title} />
          </div>
        )}
        <div className={`article__content`}>
          <h1 className={`article__title`}>{this.props.title}</h1>
          <p className={`article__description`}>{this.props.description}</p>
        </div>
      </div>
    );
  }
}

Article.propTypes = {
  /** title */
  title: PropTypes.string.isRequired,
  /** image full path*/
  image: PropTypes.string,
  /** description */
  description: PropTypes.string
};

Article.defaultProps = {
  title: "Default Article",
  image: "",
  description:
    "Donec rutrum congue leo eget malesuada. Cras ultricies ligula sed magna dictum porta. Donec sollicitudin molestie malesuada. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus"
};

export default Article;
