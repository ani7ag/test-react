import React, { Component } from "react";
import Article from "./Article";
import PropTypes from "prop-types";

/** ArticleList */
class ArticleList extends Component {
  render() {
    return (
      <div className={`articleList`}>
        {this.props.articles.length
          ? this.props.articles.map(function(article, index) {
              return (
                <div key={index}>
                  <Article {...article} />
                </div>
              );
            })
          : ""}
      </div>
    );
  }
}

ArticleList.propTypes = {
  /** articles */
  articles: PropTypes.array.isRequired
};

ArticleList.defaultProps = {
  articles: []
};

export default ArticleList;
