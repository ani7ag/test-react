import React, { Component } from "react";
import PropTypes from "prop-types";
import { headerStyles, menuStyles } from "../styles/Header.styles";

import SectionMenu from "./SectionMenu";
import SlidingPane from "react-sliding-pane";
import "react-sliding-pane/dist/react-sliding-pane.css";

let lastScrollY = 0;

/** Header */
class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showSections: true,
      onlyBurger: false,
      isMenuOpen: false
    };
  }

  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  handleScroll = () => {
    lastScrollY = window.scrollY;

    if (lastScrollY > 400) {
      this.state.showSections && this.setState({ showSections: false });
    } else {
      if (lastScrollY < 350) {
        !this.state.showSections && this.setState({ showSections: true });
      }
    }

    if (lastScrollY > 800) {
      !this.state.onlyBurger && this.setState({ onlyBurger: true });
    } else {
      if (lastScrollY < 750) {
        this.state.onlyBurger && this.setState({ onlyBurger: false });
      }
    }
  };
  render() {
    return (
      <React.Fragment>
        <div
          className={`${headerStyles(this.props)} ${!this.state.showSections &&
            `header--without-sections`} ${this.state.onlyBurger &&
            `header--only-burger`}`}
        >
          <div className="header__top_container">
            <div
              className="header__hamburger"
              onClick={() => this.setState({ isMenuOpen: true })}
            >
              <span />
              <span />
              <span />
            </div>
            <div className="header__logo">
              <img
                src={this.props.logo}
                alt="Logo"
                width={this.props.logoWidth}
                height={this.props.logoHeight}
              />
            </div>
          </div>
          {this.props.showSectionMenu && (
            <div
              className={`header__sections ${!this.state.showSections &&
                `header__sections--hidden`}`}
            >
              <SectionMenu sections={this.props.sections} />
            </div>
          )}
        </div>
        <SlidingPane
          closeIcon={<div className="menu__close" />}
          isOpen={this.state.isMenuOpen}
          from="left"
          width="200px"
          title={
            <div className="menu__logo">
              <img src={this.props.logo} alt="Logo" />
            </div>
          }
          onRequestClose={() => this.setState({ isMenuOpen: false })}
        >
          <div className={`${menuStyles}`}>
            <SectionMenu sections={this.props.sections} />
          </div>
        </SlidingPane>
      </React.Fragment>
    );
  }
}

Header.propTypes = {
  /** background color */
  background: PropTypes.string,
  /** main color */
  mainColor: PropTypes.string,
  /** logo fulpath*/
  logo: PropTypes.string,
  /** logo width*/
  logoWidth: PropTypes.number,
  /** logo height*/
  logoHeight: PropTypes.number,
  /** show section menu*/
  showSectionMenu: PropTypes.bool,
  /** sections */
  sections: PropTypes.array,
  /** section text color */
  sectionsColor: PropTypes.string,
  /** sections text size in pixels */
  sectionsTextSize: PropTypes.number
};

Header.defaultProps = {
  background: "#ffffff",
  mainColor: "#da4142",
  logo: "img/marfeel_logo_rgb.svg",
  showSectionMenu: true,
  sections: [],
  sectionsColor: "#000000",
  sectionsTextSize: 12
};

export default Header;
