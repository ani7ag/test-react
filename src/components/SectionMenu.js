import React, { Component } from "react";
import PropTypes from "prop-types";

/** SectionMenu */
class SectionMenu extends Component {
  render() {
    return (
      <div className={`sectionMenu`}>
        {this.props.sections.length
          ? this.props.sections.map(function(section, index) {
              return (
                <div key={index}>
                  <a href={section.link}>{section.title}</a>
                </div>
              );
            })
          : ""}
      </div>
    );
  }
}

SectionMenu.propTypes = {
  /** sections */
  sections: PropTypes.array.isRequired
};

SectionMenu.defaultProps = {
  sections: []
};

export default SectionMenu;
