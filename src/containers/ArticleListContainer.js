import React, { Component } from "react";
import ArticleList from "../components/ArticleList";

/** ArticleListContainer */
class ArticleListContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      articles: []
    };
  }

  componentDidMount() {
    this.fetchDataFromServer();
  }

  async fetchDataFromServer() {
    try {
      fetch("data/articles.json")
        .then(response => response.json())
        .then(json => {
          this.setState({
            articles: json
          });
        });
    } catch (error) {}
  }

  render() {
    return <ArticleList articles={this.state.articles} />;
  }
}

export default ArticleListContainer;
