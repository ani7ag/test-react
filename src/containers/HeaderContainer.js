import React, { Component } from "react";
import Header from "../components/Header";

/** HeaderContainer */
class HeaderContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      customization: []
    };
  }

  componentDidMount() {
    this.fetchDataFromServer();
  }

  async fetchDataFromServer() {
    try {
      fetch("data/header.json")
        .then(response => response.json())
        .then(json => {
          this.setState({
            customization: json
          });
        });
    } catch (error) {}
  }

  render() {
    return <Header {...this.state.customization} />;
  }
}

export default HeaderContainer;
