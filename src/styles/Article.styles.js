import { css } from "react-emotion";

const articleStyles = css`
  display: flex;
  align-items: flex-start;
  border-bottom: solid 1px #eeeeee;
  padding: 10px;

  .article__ {
    &image {
      width: 130px;
      padding-right: 10px;
    }
    &content {
    }
    &title {
      margin: 0;
      padding: 0;
      font-size: 14px;
    }
    &description {
      margin: 0;
      font-size: 12px;
    }
  }
`;

export default articleStyles;
