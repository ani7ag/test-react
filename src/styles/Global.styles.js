import { css } from "react-emotion";
import { injectGlobal } from "emotion";

const globalStyles = css``;

injectGlobal`
  .slide-pane__title-wrapper {
    margin-left: 40px;
  }
  .slide-pane__content {
    padding: 0;
  }
  .slide-pane__close {
    position: absolute;
    left: 5px;
    top: 5px;
    width: 20px;
    height: 20px;
    opacity: 1;    
    opacity: 0.5;
    margin-left: 0;

    &:before,
    &:after {
      position: absolute;
      left: 15px;
      content: " ";
      height: 20px;
      width: 2px;
      background-color: #333;
    }
    &:before {
      transform: rotate(45deg);
    }
    &:after {
      transform: rotate(-45deg);
    }
  }
`;

export default globalStyles;
