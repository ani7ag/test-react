import { css } from "react-emotion";

const headerStyles = props => css`
  background: ${props.background};
  position: sticky;
  top: 0;
  width: 100%;
  transition: height 0.5s ease;
  &.header--without-sections {
    height: 46px;
  }
  &.header--only-burger {
    width: 50px;
    border-radius: 50%;
    .header__logo {
      display: none;
    }
  }
  .header__ {
    &top_container {
      position: relative;
      background: ${props.background};
      z-index: 1;
    }
    &hamburger {
      position: absolute;
      left: 15px;
      top: 15px;
      span {
        display: block;
        width: 22px;
        height: 2px;
        margin-bottom: 5px;
        position: relative;

        background: ${props.mainColor};
        border-radius: 3px;
      }
    }
    &logo {
      height: 30px;
      padding: 8px;
      img {
        max-height: 100%;
      }
    }
    &sections {
      background: ${props.background};
      overflow: auto;
      width: 100%;
      white-space: nowrap;
      padding-bottom: 10px;
      transition: transform 0.5s ease;
      &--hidden {
        transform: translateY(-100%);
      }
      .sectionMenu > div {
        display: inline-block;
        text-transform: uppercase;
        a {
          padding: 5px 10px;
          display: block;
          text-decoration: none;
          color: ${props.sectionsColor};
          font-size: ${props.sectionsTextSize}px;
        }
        &:first-child a {
          border-bottom: solid 2px ${props.mainColor};
        }
      }
    }
  }
`;

const menuStyles = css`
  .sectionMenu div {
    a {
      color: #000000;
      text-decoration: none;
      border-bottom: solid 1px #eeeeee;
      padding: 10px;
      display: block;
      width: calc(100% - 20px);
    }
  }
`;

export { headerStyles, menuStyles };
