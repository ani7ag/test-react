import React from "react";
import ReactDOM from "react-dom";
import ArticleList from "../components/ArticleList";
import renderer from "react-test-renderer";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<ArticleList />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it("renders correctly", () => {
  const tree = renderer.create(<ArticleList />).toJSON();
  expect(tree).toMatchSnapshot();
});
