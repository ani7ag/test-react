import React from "react";
import ReactDOM from "react-dom";
import ArticleListContainer from "../containers/ArticleListContainer";
import renderer from "react-test-renderer";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<ArticleListContainer />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it("renders correctly", () => {
  const tree = renderer.create(<ArticleListContainer />).toJSON();
  expect(tree).toMatchSnapshot();
});
