import React from "react";
import ReactDOM from "react-dom";
import HeaderContainer from "../containers/HeaderContainer";
import renderer from "react-test-renderer";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<HeaderContainer />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it("renders correctly", () => {
  const tree = renderer.create(<HeaderContainer />).toJSON();
  expect(tree).toMatchSnapshot();
});
